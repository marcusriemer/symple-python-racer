#!/usr/bin/env python
import math

def deg_to_rad(deg):
    return (math.pi / 180.0) * deg

def move(point, dist, deg):
    angle = deg_to_rad(deg) * -1
    # Rotationsmatrix
    matrix = (math.cos(angle), -math.sin(angle), 0,
              math.sin(angle),  math.cos(angle), 0,
              0              ,  0              , 1)

    # Rotieren
    result = (matrix[0] * 0 + matrix[1] * 1 + matrix[2],
              matrix[3] * 0 + matrix[4] * 1 + matrix[5])

    # Skalieren
    result = (result[0] * dist, result[1] * dist)

    # Verschieben
    return (point[0] + result[0], point[1] + result[1])
    
