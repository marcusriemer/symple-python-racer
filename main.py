#!/usr/bin/env python

import pyglet
from pyglet.window import key

import point

window = pyglet.window.Window()
imageBlueCar = pyglet.resource.image('race/blue.png')
imageBlueCar.anchor_x = imageBlueCar.width / 2
imageBlueCar.anchor_y = imageBlueCar.height / 2
imageRedCar  = pyglet.resource.image('race/red.png')
imageRedCar.anchor_x = imageRedCar.width / 2
imageRedCar.anchor_y = imageRedCar.height / 2

pressed_keys = []

class Car:
    def __init__(self, image, x, y, rot):
        self._sprite = pyglet.sprite.Sprite(image)
        self._sprite.x = x
        self._sprite.y = y
        self._sprite.rotation = rot
        self._speed = 0
        self._velocity = 5
        self._turn_rate = 5

    def on_key_down(self, delta, symbol):
        if symbol == key.LEFT:
            self._sprite.rotation -= self._turn_rate
        elif symbol == key.RIGHT:
            self._sprite.rotation += self._turn_rate
        elif symbol == key.UP:
            self._speed += self._velocity
        elif symbol == key.DOWN:
            self._speed -= self._velocity

    def draw(self):
        self._sprite.draw()

    def update(self, delta):
        pos = (self._sprite.x, self._sprite.y)
        newPos = point.move(pos, delta * self._speed, self._sprite.rotation)

        self._sprite.set_position(newPos[0], newPos[1])

blueCar = Car(imageBlueCar, 50, 50, 0)

@window.event
def on_key_press(symbol, modifiers):
    if symbol in pressed_keys:
        return
    # handle pressed key
    pressed_keys.append(symbol)

@window.event
def on_key_release(symbol, modifiers):
    if symbol in pressed_keys:
        pressed_keys.remove(symbol)
    
@window.event
def on_draw():
    window.clear()    
    blueCar.draw()

def on_frame(dt):
    blueCar.update(dt)

    for key in pressed_keys:
        blueCar.on_key_down(dt, key)

pyglet.clock.schedule_interval(on_frame, 1.0 / 60.0)
pyglet.app.run()
